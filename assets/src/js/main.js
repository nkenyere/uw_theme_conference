(function(angular) {
  'use strict';

  angular
    .module('templates', [])
  ;

  angular
    .module('app', [
      'ngFx',
      'ngAnimate',
      'ngAria',
      'ngMap',
      'ui.bootstrap',
      'ui.router',
      '720kb.socialshare',
      'anim-in-out',
      'slick',
      'templates',
      'app.constants',
      'app.config',
      'app.runtime',
      'app.directives',
      'app.filters',
      'app.services',
      'app.controllers',
      'app.states',
      'app.cache'
    ])
  ;
})(window.angular);

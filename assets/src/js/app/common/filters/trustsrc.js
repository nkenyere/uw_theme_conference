(function(angular) {

  'use strict';

  angular
    .module('app.filters.trustsrc', [])
    .filter('trustsrc', function trustsrc($sce) {
      /**
       * Return public interface.
       * @param {string} url - An URL
       * @return {string} - An URL
       */
      return function(url) {
        return $sce.trustAsResourceUrl(url);
      }
    })
  ;

})(window.angular);

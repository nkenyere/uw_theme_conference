(function(angular) {
  'use strict';

  angular
    .module('app.services.siteService', [])
    .factory('siteService', function($http, SETTINGS, conferenceCache) {
      var URLS = {
        SHOW: SETTINGS.MOCK_API_URL + 'site.json'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var obj = {
        site: {}
      };

      obj.setSite = function() {
        $http
          .get(URLS.SHOW, OPTIONS)
          .success(function(data) {
            obj.site = data;
          })
        ;
      };

      obj.getSite = function() {
        return obj.site;
      };

      return obj;
    })
  ;
})(window.angular);
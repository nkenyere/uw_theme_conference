(function(angular) {
  'use strict';

  angular
    .module('app.services.metaService', [])
    .factory('metaService', function metaService($http, SETTINGS, conferenceCache) {
      var URLS = {
        SHOW: SETTINGS.LIVE_API_URL + 'conference_meta_data'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var obj = {
        meta: {
          metatags: {},
          settings: {}
        }
      };

      obj.setMeta = function() {
        $http
          .get(URLS.SHOW, OPTIONS)
          .success(function(response) {
            obj.meta = response.data;
          })
        ;
      };

      obj.getMeta = function() {
        return obj.meta;
      };

      obj.updateMetaTags = function(metaTags) {
        obj.meta.metatags = metaTags;
      }

      return obj;
    })
  ;
})(window.angular);

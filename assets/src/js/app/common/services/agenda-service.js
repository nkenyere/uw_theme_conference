(function(angular) {
  'use strict';

  angular
    .module('app.services.agendaService', [])
    .factory('agendaService', function agendaService($http, SETTINGS, conferenceCache) {
      var URLS = {
        INDEX:  SETTINGS.LIVE_API_URL + 'conference_agenda',
        SHOW:   SETTINGS.LIVE_API_URL + 'conference_agenda',
        TYPE:   SETTINGS.LIVE_API_URL + 'conference_session_types',
        TOPICS: SETTINGS.LIVE_API_URL + 'conference_session_topics' 
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var ACTIONS = {
        INDEX: function(callback) {
          $http
            .get(URLS.INDEX, OPTIONS)
            .success(function(response){
              callback(response.data);
            });
          ;
        },
        TYPE: function(callback) {
          $http
            .get(URLS.TYPE, OPTIONS)
            .success(function(response){
              callback(response.data);
            });
          ;
        },
        TOPICS: function(callback) {
          $http
            .get(URLS.TOPICS, OPTIONS)
            .success(function(response){
              callback(response.data);
            });
          ;
        },
        SHOW: function(id, callback) {
          $http
            .get(URLS.INDEX, OPTIONS)
            .success(function(data) {
              var result = null;

              _.each(response.data, function(item) {
                if (item.id == id) {
                  result = item;
                }
              });

              callback(result);
            })
          ;
        }
      };

      return {
        getAll:     ACTIONS.INDEX,
        getOne:     ACTIONS.SHOW,
        getTypes:   ACTIONS.TYPE,
        getTopics:  ACTIONS.TOPICS
      };
    })
  ;
})(window.angular);

(function(angular) {
  'use strict';

  angular
    .module('app.services.advertisementService', [])
    .factory('advertisementService', function advertisementService($http, SETTINGS, conferenceCache) {
      var URLS = {
        SHOW: SETTINGS.LIVE_API_URL + 'conference_advertisements?range=1'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var ACTIONS = {
        SHOW: function(callback) {
          $http
            .get(URLS.SHOW, OPTIONS)
            .success(function(response) {
              var result = null;

              if (response.data && response.data.length > 0) {
                result = response.data[0];
              }

              callback(result);
            })
          ;
        }
      };

      // Return the public interface
      return {
        getOne: ACTIONS.SHOW
      };
    })
  ;
})(window.angular);

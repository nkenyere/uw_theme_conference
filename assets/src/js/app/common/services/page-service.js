(function(angular, _) {
  'use strict';

  angular
    .module('app.services.pageService', [])
    .factory('pageService', function pageService($http, $rootScope, SETTINGS, conferenceCache) {
      var URLS = {
        SHOW:  SETTINGS.LIVE_API_URL + 'conference_web_pages',
        FORM:  SETTINGS.LIVE_API_URL + 'conference_web_forms'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var ACTIONS = {
        SHOW: function(slug, callback) {
          if (!slug) {
            slug = '<front>'
          }

          $http
            .get(URLS.SHOW + '/' + slug, OPTIONS)
            .success(function(response) {
              var result = null;

              if (response.data && response.data.length > 0) {
                result = response.data[0];
              }

              callback(result);
            })
            .error(function() {
              $http
                .get(URLS.FORM + '/' + slug, OPTIONS)
                .success(function(response) {
                  if (response.data && response.data.length > 0) {
                    callback(response.data[0]);
                  }
                })
                .error(function() {
                  callback();
                })
              ;
            })
          ;
        }
      };

      return {
        getOne: ACTIONS.SHOW
      };
    })
  ;
})(window.angular, window._);

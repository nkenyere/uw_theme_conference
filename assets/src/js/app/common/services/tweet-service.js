(function(angular, twttr) {
  'use strict';

  angular
    .module('app.services.tweetService', [])
    .factory('tweetService', function tweetService($http, SETTINGS, conferenceCache) {
      var URLS = {
        INDEX: SETTINGS.LIVE_API_URL + 'conference_tweets'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var getAll = function(callback) {
        $http
          .get(URLS.INDEX, OPTIONS)
          .success(function(response) {
            var tweets  = response.data;
            var results = [];

            if (tweets && tweets.length > 0) {
              for (var i = 0; i < tweets.length; i++) {
                var tweet  = tweets[i];
                tweet.text = twttr.txt.autoLink(twttr.txt.htmlEscape(tweet.text));
                results.push(tweet);
              }
            }

            callback(results);
          })
        ;
      };

      /**
       * Return the public interface.
       */
      return {
        getAll: getAll
      }
    })
  ;
})(window.angular, window.twttr);

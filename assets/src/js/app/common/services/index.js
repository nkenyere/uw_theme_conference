(function(angular) {
  'use strict';

  angular
    .module('app.services', [
      'app.services.advertisementService',
      'app.services.agendaService',
      'app.services.blogService',
      'app.services.ckeditorSocialMediaService',
      'app.services.filterService',
      'app.services.linksService',
      'app.services.menuService',
      'app.services.metaService',
      'app.services.pageService',
      'app.services.siteService',
      'app.services.speakerService',
      'app.services.sponsorService',
      'app.services.tweetService',
      'app.services.videoService'
    ])
  ;
})(window.angular);

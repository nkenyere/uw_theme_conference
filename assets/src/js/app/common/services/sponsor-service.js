(function(angular) {
  'use strict';

  angular
    .module('app.services.sponsorService', [])
    .factory('sponsorService', function sponsorService($http, SETTINGS, conferenceCache) {
      var URLS = {
        INDEX:  SETTINGS.LIVE_API_URL + 'conference_sponsors',
        LEVELS: SETTINGS.LIVE_API_URL + 'conference_sponsorship_levels'
      };

      var OPTIONS = {
        cache: conferenceCache
      };

      var ACTIONS = {
        INDEX: function(callback) {
          $http
            .get(URLS.INDEX, OPTIONS)
            .success(function(response){
              callback(response.data);
            })
          ;
        },
        LEVELS: function(callback) {
          $http
            .get(URLS.LEVELS, OPTIONS)
            .success(function(response){
              callback(response.data);
            })
          ;
        }
      };

      return {
        getAll:    ACTIONS.INDEX,
        getLevels: ACTIONS.LEVELS
      };
    })
  ;
})(window.angular);

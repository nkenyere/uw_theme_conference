(function(angular, _) {
  'use strict';

  angular
    .module('app.controllers.siteController', [
      'app.services.menuService',
      'app.services.metaService',
      'app.services.siteService'
    ])
    .controller('siteController', function siteController($rootScope, menuService, metaService, siteService, $window) {
      menuService.setMenu();
      metaService.setMeta();
      siteService.setSite();

      $rootScope.menu       = menuService.getMenu;
      $rootScope.meta       = metaService.getMeta;
      $rootScope.site       = siteService.getSite;
      $rootScope.menuShown  = false;

      $rootScope.mobileMenu = function(link) {
        $rootScope.menuShown  = !$rootScope.menuShown;
        if(link != 'trigger') {
          $window.location.href = '/' + link;
        }
      };

      $rootScope.getAgendaPage = function() {
        return getAliasForFeature('agenda');
      }

      $rootScope.getBlogPage = function() {
        return getAliasForFeature('blog');
      }

      $rootScope.getSponsorsPage = function() {
        return getAliasForFeature('sponsors');
      }

      $rootScope.getSpeakersPage = function() {
        return getAliasForFeature('speakers');
      }

      $rootScope.getVideosPage = function() {
        return getAliasForFeature('videos');
      }

      function getAliasForFeature(feature) {
        var result = '';
        _.each($rootScope.menu().items, function(item) {
          if (item.page_feature == feature) {
            result = item.alias;
          }
        });
        return result;
      }
    })
  ;
})(window.angular, window._);

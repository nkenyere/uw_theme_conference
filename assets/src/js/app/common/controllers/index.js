(function(angular) {
  'use strict';

  angular
    .module('app.controllers', [
      'app.controllers.siteController'
    ])
  ;
})(window.angular);
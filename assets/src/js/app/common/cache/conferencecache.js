(function(angular, _) {
  'use strict';

  angular
    .module('app.cache.conferenceCache', [])
    .factory('conferenceCache', function conferenceCache($cacheFactory) {
      return $cacheFactory('conferenceCache');
    })
  ;
})(window.angular, window._);

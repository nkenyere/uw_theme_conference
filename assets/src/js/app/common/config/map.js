(function(angular) {
  angular
    .module('app.config.map', [])
    .config(function(NgMapProvider) {
      NgMapProvider
        .setDefaultOptions({
          zoom: 14,
          scrollwheel: false,
          navigationControl: false,
          mapTypeControl: false,
          scaleControl: false,
          draggable: false,
          disableDefaultUI: false
        })
      ;
    })
  ;
})(window.angular);

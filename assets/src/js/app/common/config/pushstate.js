(function(angular) {
  'use strict';

  angular
    .module('app.config.pushstate', [])
    .config(function($locationProvider) {
      // Do not intercept URLS
      $locationProvider.html5Mode({
        enabled: true
      });

      $locationProvider.hashPrefix('');
    })
  ;
})(window.angular);

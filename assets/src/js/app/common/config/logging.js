(function(angular) {
  'use strict';

  angular
    .module('app.config.logging', [])
    .config(function($logProvider) {
      $logProvider.debugEnabled(true);
    })
  ;
})(window.angular);

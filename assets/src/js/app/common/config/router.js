(function(angular) {
  'use strict'; 

  angular
    .module('app.config.router', [
      'ui.router',
      'app.services.sponsorService'
    ])
    .config(function($stateProvider, $urlRouterProvider, SETTINGS) {
      // Default route if no route matched
      $urlRouterProvider.otherwise('/404');

      // Abstract controller for the app. Here be global logic.
      $stateProvider
        .state('app', {
          url: '',
          abstract: true,
          views: {
            'sponsors@': {
              templateUrl: SETTINGS.TEMPLATE_URL + '/components/sponsors.tpl.html',
              controller: function($scope, sponsorService) {
                $scope.vm = {};
                $scope.vm.sponsors = [];

                sponsorService
                  .getAll(function(data) {
                    $scope.vm.sponsors = data;
                  })
                ;
              }
            }
          }
        })
      ;
    })
  ;

})(window.angular);

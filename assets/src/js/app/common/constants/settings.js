(function(angular) {
  'use strict';

  angular
    .module('app.constants.settings', [])
    .constant('SETTINGS', {
      TEMPLATE_URL: '/apps/uw_theme_conference/templates/',
      THEME_URL:    '/apps/uw_theme_conference/',
      MOCK_API_URL: '/apps/uw_theme_conference/data/',
      LIVE_API_URL: 'api/v1.0/'
    })
  ;
})(window.angular);

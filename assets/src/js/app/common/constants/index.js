(function(angular) {
  'use strict';

  angular
    .module('app.constants', [
      'app.constants.settings'
    ])
  ;
})(window.angular);
(function(angular) {
  'use strict';

  angular
    .module('app.directives.featuredspeakers', [])
    .directive('featuredspeakers', function(SETTINGS) {
      var featuredSpeakersController = function(speakerService, $rootScope, $scope, $modal) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Show additional speaker details in a modal window.
         * @param {object} speaker - A speaker instance.
         */
        vm.showSpeaker = function(speaker) {
          var modalInstance = $modal.open({
            animate: true,
            templateUrl: SETTINGS.TEMPLATE_URL + 'components/speaker.modal.tpl.html',
            controller: function($scope, $modalInstance, speaker) {
              $scope.speaker = angular.copy(speaker);
              $scope.close = function() {
                $modalInstance.dismiss('cancel');
              };
            },
            size: 'lg',
            resolve: {
              speaker: speaker
            }
          });
        };

        /**
         * Set the initial state of this controller.
         */
        function setInitialState() {
          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.speakers) {
              $scope.speakers = newValue.speakers;
            }
          });

          $scope.$watch('vm.dataclasses', function(newValue, oldValue) {
            vm.classes = angular.copy(newValue);
          });

          $scope.$watch('vm.datalayout', function(newValue, oldValue) {
            vm.layout = angular.copy(newValue);
          });

          $scope.getSpeakersPage = $rootScope.getSpeakersPage;
          vm.featuredSpeakers    = [];

          loadSpeakers();
        }

        /**
         * Load speakers via speakerService and store in the view-model.
         */
        function loadSpeakers() {
          vm.loading = true;
          vm.error   = null;

          speakerService
            .getFeatured(4, function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No speakers found';
              }

              if (data.length > 4) {
                vm.showMore = true;
                data.pop();
                data.pop();
              }

              vm.featuredSpeakers = data;
              vm.loading = false;
            })
          ;
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        controller: featuredSpeakersController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/featuredspeakers.tpl.html'
      };
    })
  ;
})(window.angular);

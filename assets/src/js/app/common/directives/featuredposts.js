(function(angular) {
  'use strict';

  angular
    .module('app.directives.featuredposts', [])
    .directive('featuredposts', function(SETTINGS) {

      var featuredPostsController = function(blogService, $rootScope, $scope) {
        var vm = this;

        vm.getBlogPage = $rootScope.getBlogPage;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.blog) {
              $scope.blog = newValue.blog;
            }
          });

          $scope.$watch('vm.dataclasses', function(newValue, oldValue) {
            vm.classes = angular.copy(newValue);
          });

          $scope.$watch('vm.datalayout', function(newValue, oldValue) {
            vm.layout = angular.copy(newValue);
          });

          vm.featuredPosts = [];

          loadFeaturedPosts();
        }

        /**
         * Load featured posts and store them in the view-model.
         */
        function loadFeaturedPosts() {
          vm.loading = true;
          vm.error   = null;

          blogService
            .getRecent(4, function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No speakers found';
              }

              vm.featuredPosts = data;
              vm.loading = false;
            })
          ;
        }
      };

      return {
        restrict: 'EA',
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        controller: featuredPostsController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/featuredposts.tpl.html'
      };
    })
  ;
})(window.angular);

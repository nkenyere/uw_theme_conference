(function(angular) {
  'use strict';

  angular
    .module('app.directives.featuredagenda', [])
    .directive('featuredagenda', function(SETTINGS) {
      /**
       * The controller for this directive.
       * @param {object} $rootScope - The $rootScope service.
       * @param {object} $scope - The $scope service.
       */
      var featuredAgendaDirectiveController = function(agendaService, $rootScope, $scope) {
        // Initialize the controller.
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.agenda) {
              $scope.agenda = newValue.agenda;
            }
          });

          $scope.$watch('dataclasses', function(newValue, oldValue) {
            $scope.classes = angular.copy(newValue);
          });

          $scope.$watch('datalayout', function(newValue, oldValue) {
            $scope.layout = angular.copy(newValue);
          });

          $scope.schedules     = [];
          $scope.loading       = true;
          $scope.error         = null;
          $scope.getAgendaPage = $rootScope.getAgendaPage;

          loadAgenda();
        }

        /**
         * Load agenda via agendaService and store in the view-model.
         * Provide error message on view-model to use if no sessions found.
         * @param {object} vm - The view model instance.
         */
        function loadAgenda(vm) {
          agendaService
            .getAll(function(data) {

              if (!data || data.length < 1) {
                $scope.error = 'No agenda found';
              }

              $scope.schedules = data;
              $scope.loading   = false;
            });
          ;
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: featuredAgendaDirectiveController,
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/featuredagenda.tpl.html'
      }
    })
  ;
})(window.angular);

(function(angular) {
  'use strict';

  /**
   * The speaker groups directive encapsulates the logic of displaying
   * speaker groups. This directive includes a feature for displaying additional
   * speaker details in a modal window.
   */
  angular
    .module('app.directives.speakergroups', [])
    .directive('speakergroups', function(SETTINGS) {
      var speakerGroupsDirectiveController = function(speakerService, $modal, $scope) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Show additional speaker details in a modal window.
         * @param {object} speaker - A speaker instance.
         */
        vm.showSpeaker = function(speaker) {
          var modalInstance = $modal.open({
            animate: true,
            templateUrl: SETTINGS.TEMPLATE_URL + 'components/speaker.modal.tpl.html',
            controller: function($scope, $modalInstance, speaker) {
              $scope.speaker = angular.copy(speaker);
              $scope.close = function() {
                $modalInstance.dismiss('cancel');
              };
            },
            size: 'lg',
            resolve: {
              speaker: speaker
            }
          });
        };

        /**
         * Set the initial state of this controller.
         */
        function setInitialState() {
          vm.speakerGroups = [];
          vm.loading = false;
          vm.error   = null;

          loadSpeakerGroups(vm);
        }

        /**
         * Load speaker groups via speakerService and store in the view model.
         */
        function loadSpeakerGroups() {
          speakerService
            .getGroups(function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No speakers found';
              }

              vm.speakerGroups = data;
              vm.loading = false;
            })
          ;
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: speakerGroupsDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/speaker-groups.tpl.html'
      }
    })
  ;
})(window.angular);

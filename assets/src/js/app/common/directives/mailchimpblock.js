(function(angular) {
  'use strict';

  angular
    .module('app.directives.mailchimpblock', [])
    .directive('mailchimpblock', function(SETTINGS) {
      /**
       * The controller for this directive.
       * @param {object} $scope - The $scope service
       */
      var mailchimpBlockDirectiveController = function($rootScope, $scope) {
        // Initialize the controller.
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $scope.meta = $rootScope.meta;

          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.mailchimp_block) {
              $scope.mailchimpBlock = newValue.mailchimp_block;
            }
          });

          $scope.$watch('dataclasses', function(newValue, oldValue) {
            $scope.classes = angular.copy(newValue);
          });

          $scope.$watch('datalayout', function(newValue, oldValue) {
            $scope.layout = angular.copy(newValue);
          });
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: mailchimpBlockDirectiveController,
        scope: {
          dataclasses: '@',
          datalayout: '@',
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/mailchimpblock.tpl.html'
      }
    })
  ;
})(window.angular);

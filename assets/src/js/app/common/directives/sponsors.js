(function(angular, _) {
  'use strict';

  /**
   * The sponsors directive encapsulates the logic of displaying sponsors.
   */
  angular
    .module('app.directives.sponsors', [])
    .directive('sponsors', function(SETTINGS) {
      var sponsorsDirectiveController = function(sponsorService) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Set the initial state of this controller.
         */
        function setInitialState() {
          vm.levels  = [];
          vm.loading = true;
          vm.error   = null;

          loadLevels();
        }

        /**
         * Load the sponsorship levels via sponsorService and store in the
         * view-model.
         */
        function loadLevels() {
          sponsorService
            .getLevels(function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No sponsors found';
              }

              vm.levels  = data;
              vm.loading = false;
              loadOthers();
            })
          ;
        }
        
        /**
         * Add an "other" level to to the view-model.
         */
        function loadOthers() {
          sponsorService  
            .getAll(function(data) {
              var otherSponsors = [];
              
              _.each(data, function(sponsor) {
                if (!sponsorExists(sponsor.id)) {
                  otherSponsors.push(sponsor);
                }
              });
              
              var otherLevel = {
                sponsors: otherSponsors
              };
              
              if (vm.levels.length > 0) {
                otherLevel.name = 'Other';
                otherLevel.label = 'Other';
              }
              
              if (otherSponsors.length > 0) {
                vm.levels.push(otherLevel);
              }
            })
          ;
        }
        
        /**
         * Check is sponsor exists. 
         */
        function sponsorExists(id) {
          var found = false;
          
          if (vm.levels.length > 0) {
            _.each(vm.levels, function(level) {
              if (level.sponsors.length > 0) {
                _.each(level.sponsors, function(sponsor) {
                  if (sponsor.id == id) {
                    found = true;
                  }
                });
              }
            });
          }
          
          return found;
        }
      };

      /**
       * Return the public interface of the directive.
       */
      return {
        restrict: 'EA',
        controller: sponsorsDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/sponsors.tpl.html'
      }
    })
  ;
})(window.angular, window._);

(function(angular) {
  'use strict';

  angular
    .module('app.directives.location', [])
    .directive('location', function(SETTINGS) {
      /**
       * Controller for the directive.
       * @param {object} - The $rootScope service.
       */
      var locationDirectiveController = function(NgMap, $rootScope, $scope, $timeout) {
        var vm = this;

        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $scope.mapLocation   = {};
          $scope.map = {
            object: null,
            options: {},
            marker: null
          };

          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.location) {
              $scope.mapLocation = newValue.location;

              NgMap
                .getMap()
                .then(function(map) {
                  renderMap(map);
                })
              ;
            }
          });

          $scope.$watch('dataclasses', function(newValue, oldValue) {
            $scope.classes = angular.copy(newValue);
          });

          $scope.$watch('datalayout', function(newValue, oldValue) {
            $scope.layout = angular.copy(newValue);
          });
        }

        /**
         * Render marker on the map. Depends on location and map being previously hydrated.
         */
        function renderMap(map) {
          $scope.map.object  = map;
          $scope.map.object.setOptions({
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
            disableDefaultUI: true
          });

          var markerOptions = {
            map: $scope.map.object,
            position: new google.maps.LatLng($scope.mapLocation.latitude, $scope.mapLocation.longitude)
          };

          if ($scope.mapLocation.marker_icon) {
            markerOptions.icon = $scope.mapLocation.marker_icon;
          }

          if ($scope.mapLocation.address) {
            markerOptions.html = '<div class="info-window">' + $scope.mapLocation.address + '</div>';
            $scope.map.infoWindow = new google.maps.InfoWindow({content: ""});
          }

          $scope.map.marker = new google.maps.Marker(markerOptions);
          google.maps.event.addListener($scope.map.object, 'resize', function() {
            $scope.map.object.setCenter(new google.maps.LatLng($scope.mapLocation.latitude, $scope.mapLocation.longitude));
          });

          if ($scope.map.marker.html) {
            google.maps.event.addListener($scope.map.marker, 'click', function() {
              $scope.map.infoWindow.setContent(this.html);
              $scope.map.infoWindow.open($scope.map.object, this);
            });
          }

          $timeout(function() {
            google.maps.event.trigger($scope.map.object, 'resize');
          }, 3000);
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: locationDirectiveController,
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/location.tpl.html'
      }
    })
  ;
})(window.angular);

(function(angular) {
  'use strict';

  angular
    .module('app.directives.tint', [])
    .directive('tint', function(SETTINGS) {
      /**
       * The controller for this directive.
       * @param {object} $scope - The $scope service
       * @param {object} videoService - The videoService
       */
      var tintDirectiveController = function($rootScope, $scope) {
        // Initialize the controller.
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.tint) {
              $scope.tint = newValue.tint;
            }
          });

          $scope.$watch('dataclasses', function(newValue, oldValue) {
            $scope.classes = angular.copy(newValue);
          });

          $scope.$watch('datalayout', function(newValue, oldValue) {
            $scope.layout = angular.copy(newValue);
          });
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: tintDirectiveController,
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/tint.tpl.html'
      }
    })
  ;
})(window.angular);

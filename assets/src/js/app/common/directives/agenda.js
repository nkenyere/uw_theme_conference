(function(angular) {
  'use strict';

  /**
   * The agenda directive encapsulates the logic of displaying and filtering
   * agendas.
   *
   * An agenda consists of multiple schedules with multiple sessions.
   * An agenda is tabbed by date and filterable by type and topic.
   */
  angular
    .module('app.directives.agenda', [])
    .directive('agenda', function(SETTINGS) {
      var agendaDirectiveController = function(agendaService, filterService, $anchorScroll, $location, $stateParams, $timeout) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Set the date. Used for filtering list of sessions.
         * @param {string} date - The schedule date.
         */
        vm.searchByDate = function(date) {
          vm.search.date = date;
        };

        /**
         * Set the type. Used for filtering list of sessions.
         * @param {number} tid - The term id.
         */
        vm.searchByType = function(tid) {
          vm.search.type = (vm.search.type == tid) ? '' : tid;
        };

        /**
         * Set the topic. Used for filtering list of sessions.
         * @param {number} tid - The term id.
         */
        vm.searchByTopic = function(tid) {
          vm.search.topic = (vm.search.topic == tid) ? '' : tid;
        };

        /**
         * Callback on ngFilter to filter out sessions that do not have given date.
         * @param {Object} input - A session.
         * @return {Object|undefined} - Returns session if not filtered out.
         */
        vm.filterByDate = function(input) {
          if (!vm.search.date || vm.search.date == input.date) {
            return input;
          }
        };

        /**
         * Callback on ngFilter to filter out sessions that do not have given term.
         * @param {Object} input - A session
         * @return {Object|undefined} - Returns session if not filtered out.
         */
        vm.filterByType = function(input) {
          if (!vm.search.type) {
            return input;
          }

          return filterService.filterByTerm(input, 'type', vm.search.type);
        };

        /**
         * Callback on ngFilter to filter out sessions that do not have given term.
         * @param {Object} input - A session
         * @return {Object|undefined} - Returns session if not filtered out.
         */
        vm.filterByTopic = function(input) {
          if (!vm.search.topic) {
            return input;
          }

          return filterService.filterByTerms(input, 'topics', vm.search.topic);
        }

        /**
         * Set the initial state of this controller.
         */
        function setInitialState() {
          vm.agenda  = [];
          vm.loading = true;
          vm.error   = null;

          vm.search = {
            date: "",
            type: ""
          };

          vm.sessionTypes = [];
          vm.sessionTopics = [];
          vm.activeSession = null;

          preloadSession(vm);
          loadAgenda(vm);
          loadSessionTypes(vm);
          loadSessionTopics(vm);
        }

        /**
         * Prepload a session if the id is in the params.
         *
         */
        function preloadSession() {
          vm.activeSession = $location.search().id;
          setActiveSchedule();

          if (vm.activeSession) {
            $timeout(function() {
              $anchorScroll('session-id' + session.id);
            }, 300);
          }
        }

        /**
         * Load agenda via agendaService and store in the view-model.
         * Provide error message on view-model to use if no sessions found.
         * @param {object} vm - The view model instance.
         */
        function loadAgenda(vm) {
          agendaService
            .getAll(function(data) {

              if (!data || data.length < 1) {
                vm.error = 'No agenda found';
              }

              vm.agenda  = data;
              vm.loading = false;
            });
          ;
        }

        /**
         * Load session types via agendaService and store in the view-model.
         * @param {object} vm - The view model instance.
         */
        function loadSessionTypes(vm) {
          agendaService
            .getTypes(function(data) {
              vm.sessionTypes = data;
            })
          ;
        }

        /**
         * Load session topics via agendaService and store in the view-model.
         * @param {object} vm - The view model instance.
         */
        function loadSessionTopics(vm) {
          agendaService
            .getTopics(function(data) {
              vm.sessionTopics = data;
            })
          ;
        }

        /**
         * Set the active schedule. Defaults to all.
         */
        function setActiveSchedule() {
          if (vm.agenda.schedules && vm.agenda.schedules.length > 0 && vm.agenda.schedules[0].sessions && vm.agenda.schedules[0].sessions.length > 0) {
            vm.search.date = vm.agenda.schedules[0].sessions[0].date;
          }

          if (vm.activeSession) {
            _.forEach(vm.agenda.schedules, function(schedule) {
              _.forEach(schedule.sessions, function(session) {
                if (vm.activeSession == session.id) {
                  vm.search.date = session.date;
                }
              });
            });
          }
        }

      };

      /**
       * Example usage:
       *  <div agenda datasource=vm.agenda datatypes=vm.types datatopics=vm.topics></div>
       */
      return {
        restrict: 'EA',
        controller: agendaDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/agenda.tpl.html'
      }
    })
  ;
})(window.angular);

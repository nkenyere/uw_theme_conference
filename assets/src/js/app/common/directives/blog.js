(function(angular, _) {
  'use strict';

  /**
   * The blog directive encapsulates the logic of displaying and filtering blog
   * posts.
   */
  angular
    .module('app.directives.blog', [])
    .directive('blog', function(SETTINGS) {
      var blogDirectiveController = function(blogService, $location, $rootScope, $scope, $stateParams) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Filter posts by search term. Each search performed should reset the
         * posts list and pagination information.
         */
        vm.search = function() {
          var prev = vm.term;
          vm.term  = vm.tempTerm;

          if (vm.term != prev) {
            var filters = {};

            if (vm.category) {
              filters.category = vm.category;
            }

            resetCollectionState();
            searchPosts(vm.term, filters);
          }
        };

        /**
         * Filter posts by category.
         */
        vm.searchByCategory = function(name) {
          if (!name || vm.category == name) {
            vm.category = null;
            $location.search('category', null);
            resetCollectionState();
            loadPosts(); // Same as reset;
          } else {
            vm.category = name;
            $location.search('category', name);
            resetCollectionState();
            filterPosts(name);
          }
        };

        /**
         * Load more
         */
        vm.loadMore = function() {
          if (vm.term) {
            var filters = {};

            if (vm.category) {
              filters.category = vm.category;
            }

            searchPosts(vm.term, filters);
          } else if (vm.category) {
            filterPosts(vm.category);
          } else {
            loadPosts();
          }
        }

        /**
         * Resets the state of the collection.
         */
        function resetCollectionState() {
          vm.posts  = [];
          vm.page   = 1;
          vm.count  = 0;
          vm.loaded = 0;
        }

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          vm.posts        = [];
          vm.featuredPost = {};
          vm.categories   = [];
          vm.excluded     = null;

          // For search and filtering...
          vm.term     = '';
          vm.tempTerm = '';
          vm.category = '';

          // For pagination
          vm.limit    = 10;
          vm.page     = 1;
          vm.count    = 0;
          vm.loaded   = 0;

          loadCategories();
          loadFeaturedPost();

          $scope.getBlogPage = $rootScope.getBlogPage;
        }

        /**
         * Load posts via blogService and store in the view-model.
         */
        function loadPosts() {
          vm.loading = false;
          vm.error   = null;

          var params = {
            limit: vm.limit,
            page: vm.page
          };

          // Optionally exlude nodes, such as featured node.
          if (vm.excluded) {
            params.exclude = vm.excluded;
          }

          blogService
            .getAll(params, function(response) {
              if (!response.data || response.data.length < 1) {
                vm.error = 'No posts found';
              }

              vm.posts   = vm.posts.concat(response.data);
              vm.count   = response.count;
              vm.page    = vm.page + 1;
              vm.loaded  = vm.posts.length;
              vm.loading = false;
            })
          ;
        }

        /**
         * Search posts via blogService and store in the view-model.
         * @param {string} term - The search term.
         * @param {object} filters - Additional filtering parameters for query.
         */
        function searchPosts(term, filters) {
          vm.loading = true;
          vm.error   = null;

          var params = {
            limit: vm.limit,
            page: vm.page
          };

          // Optionally eclude nodes, such as featured node.
          if (vm.excluded) {
            params.exclude = vm.excluded;
          }

          blogService
            .getAllByTerm(params, term, filters, function(response) {
              if (!response.data || response.data.length < 1) {
                vm.error = 'No posts found';
              }

              vm.posts   = vm.posts.concat(response.data);
              vm.count   = response.count;
              vm.page    = vm.page + 1;
              vm.loaded  = vm.posts.length;
              vm.loading = false;
            })
          ;
        }

        /**
         * Filter posts by category via blogService and store in the view-model.
         * @param {string} name - The category name.
         */
        function filterPosts(name) {
          vm.loading = true;
          vm.error   = null;

          var params = {
            limit: vm.limit,
            page: vm.page
          };

          blogService
            .getAllByCategory(params, name, function(response) {
              if (!response.data || response.data.length < 1) {
                vm.error = 'No posts found';
              }

              vm.posts   = vm.posts.concat(response.data);
              vm.count   = response.count;
              vm.page    = vm.page + 1;
              vm.loaded  = vm.posts.length;
              vm.loading = false;
            })
          ;
        }

        /**
         * Load featured post via blogService and store in the view-model.
         */
        function loadFeaturedPost() {
          blogService
            .getFeatured(1, function(data) {
              if (data && data.length > 0) {
                vm.featuredPost = data[0];
                vm.excluded = vm.featuredPost.id; // Featured post will be excluded from all future queries.
              }

              var category = $location.search().category;

              if (category) {
                vm.category = category;
                filterPosts(category);
              } else {
                loadPosts(); // Exclude featured post from main list.
              }
            })
          ;
        }

        /**
         * Load categories via blogService and store in the view-model.
         */
        function loadCategories() {
          blogService
            .getCategories(function(data) {
              vm.categories = data;
            })
          ;
        }
      }

      return {
        restrict: 'EA',
        controller: blogDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/blog.tpl.html'
      }
    })
  ;
})(window.angular, window._);

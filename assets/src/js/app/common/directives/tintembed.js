(function(angular) {
  angular
    .module('app.directives.tintembed', [])
    .directive('tintembed', function(SETTINGS, $compile, $rootScope) {
      /**
       * Controller for the directive.
       * @param {object} $scope - The $scope service.
       */
      var tintEmbedDirectiveController = function($scope) {

      }

      /**
       * Link callback for the directive.
       * @param {object} scope - The directive scope.
       * @param {object} element - The directive element.
       * @param {object} attrs - The directive element attributes.
       */
      var tintEmbedDirectiveLink = function(scope, element, attrs) {
        $rootScope.$watch('meta().settings', function(newValue, oldValue) {
          if (newValue && newValue.tint) {
            var tint = newValue.tint;
            var html = '';

            html += '<script src="https://d36hc0p18k1aoc.cloudfront.net/public/js/modules/tintembed.js" type="text/javascript-lazy"></script>';
            html += '<div class="' + tint.class + '" data-id="' + tint.id + '" data-columns="' + tint.columns + '" data-mobile-scroll="true" data-infinitescroll="true" style="height:' + tint.height + ';width:' + tint.width + ';"></div>';

            html = $compile(html)(scope);
            element.after(html);
          }
        });
      }

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: tintEmbedDirectiveController,
        link: tintEmbedDirectiveLink
      }
    })
  ;
})(window.angular);

(function(angular) {
  'use strict';

  angular
    .module('app.directives.links', [])
    .directive('links', function(SETTINGS) {
      var linksDirectiveController = function(linksService, $rootScope, $scope) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.links) {
              $scope.links = newValue.links;
            }
          });

          $scope.$watch('vm.dataclasses', function(newValue, oldValue) {
            vm.classes = angular.copy(newValue);
          });

          $scope.$watch('vm.datalayout', function(newValue, oldValue) {
            vm.layout = angular.copy(newValue);
          });

          vm.links = [];

          loadLinks();
        }

        /**
         * Load links and store them in the view-model.
         */
        function loadLinks() {
          vm.loading = true;
          vm.error   = null;

          linksService
            .getAll(function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No links found';
              }

              vm.links   = (data.length && data.length > 0) ? data[0] : [];
              vm.loading = false;
            })
          ;
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: linksDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/links.tpl.html'
      }
    })
  ;
})(window.angular);

(function(angular) {
  'use strict';

  angular
    .module('app.states.page', [
      'ui.router',
      'app.states.page.controllers'
    ])
    .config(function($stateProvider, SETTINGS) {
      // Module states
      var states = {
        index: {
          url: '',
          views: {
            'main@': {
              templateUrl: SETTINGS.TEMPLATE_URL + 'pages/page.tpl.html',
              controller: 'pageShowController as vm'
            }
          }
        },
        show: {
          url: '/:slug',
          views: {
            'main@': {
              templateUrl: SETTINGS.TEMPLATE_URL + 'pages/page.tpl.html',
              controller: 'pageShowController as vm'
            }
          }
        }
      };

      $stateProvider.state('app.home', states.index);
      $stateProvider.state('app.page', states.show);
    })
  ;
})(window.angular);

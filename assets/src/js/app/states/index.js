(function(angular) {
  'use strict';

  angular
    .module('app.states', [
      'app.states.blog',
      'app.states.page'
    ])
  ;
})(window.angular);

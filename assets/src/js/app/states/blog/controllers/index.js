(function(angular) {
  'use strict';

  angular
    .module('app.states.blog.controllers', [
      'app.states.blog.controllers.blogShowController'
    ])
  ;
})(window.angular);

(function(angular) {
  'use strict';

  angular
    .module('app.states.blog', [
      'ui.router',
      'app.states.blog.controllers'
    ])
    .config(function($stateProvider, SETTINGS) {
      // Module states
      var states = {
        show: {
          url: '/blog/post/:slug',
          views: {
            'main@': {
              templateUrl: SETTINGS.TEMPLATE_URL + 'pages/post.tpl.html',
              controller: 'blogShowController as vm'
            }
          }
        }
      };

      $stateProvider.state('app.post', states.show);
    })
  ;
})(window.angular);

(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var livestreamDialog = function(editor) {
    return {
      title : 'Livestream Properties',
      minWidth : 625,
      minHeight : 100,
      contents: [{
        id: 'livestream',
        label: 'livestream',
        elements:[{
          type: 'text',
          id: 'livestreamInput',
          label: 'Livestream channel page name: original.livestream.com/',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-username'));
                 }
        },{
          type: 'text',
          id: 'displaynameInput',
          label: 'Display name:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-displayname'));
                 }
        }]
      }],
      onOk: function() {
        //get form information
        livestreamInput = this.getValueOf('livestream','livestreamInput');
        displaynameInput = this.getValueOf('livestream','displaynameInput');
        //validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!CKEDITOR.socialmedia.livestream_username_regex.test(livestreamInput)) {
          errors += "You must enter a valid Livestream channel page name.\r\n";
        }
        if (!displaynameInput) {
          errors += "You must enter a display name.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          //create the cklivestream element
          var cklivestreamNode = new CKEDITOR.dom.element('cklivestream');
          //save contents of dialog as attributes of the element
          cklivestreamNode.setAttribute('data-username',livestreamInput);
          cklivestreamNode.setAttribute('data-displayname',displaynameInput);
          //adjust title based on user input
          CKEDITOR.lang.en.fakeobjects.cklivestream = CKEDITOR.socialmedia.cklivestream+': '+displaynameInput;
          //create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(cklivestreamNode, 'cklivestream', 'cklivestream', false);
      newFakeImage.addClass('cklivestream');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          //reset title
          CKEDITOR.lang.en.fakeobjects.cklivestream = CKEDITOR.socialmedia.cklivestream;
        }
      },
      onShow: function() {
        //Set up to handle existing items
        this.fakeImage = this.cklivestreamNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cklivestream') {
          this.fakeImage = fakeImage;
          var cklivestreamNode = editor.restoreRealElement(fakeImage);
          this.cklivestreamNode = cklivestreamNode;
          this.setupContent(cklivestreamNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('livestream', function(editor) {
    return livestreamDialog(editor);
  });    
})();

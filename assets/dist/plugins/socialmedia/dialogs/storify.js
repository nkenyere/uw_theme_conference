(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var storifyDialog = function(editor) {
    return {
      title : 'Storify Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'storify',
        label: 'storify',
        elements:[{
          type: 'text',
          id: 'storifyInput',
          label: 'Storify user name:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-username'));
                 }
        },{
          type: 'text',
          id: 'storyInput',
          label: 'Storify story URL fragment:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-story'));
                 }
        },{
          type: 'text',
          id: 'displaynameInput',
          label: 'Display name:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-displayname'));
                 }
        }]
      }],
      onOk: function() {
        //get form information
        storifyInput = this.getValueOf('storify','storifyInput');
        storyInput = this.getValueOf('storify','storyInput');
        displaynameInput = this.getValueOf('storify','displaynameInput');
        //validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!storifyInput) {
          errors += "You must enter a Storify user name.\r\n";
        }
        if (!storyInput) {
          errors += "You must enter a Storify user name.\r\n";
        }
        if (!displaynameInput) {
          errors += "You must enter a display name.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          //create the ckstorify element
          var ckstorifyNode = new CKEDITOR.dom.element('ckstorify');
          //save contents of dialog as attributes of the element
          ckstorifyNode.setAttribute('data-username',storifyInput);
          ckstorifyNode.setAttribute('data-story',storyInput);
          ckstorifyNode.setAttribute('data-displayname',displaynameInput);
          //adjust title based on user input
          CKEDITOR.lang.en.fakeobjects.ckstorify = CKEDITOR.socialmedia.ckstorify+': '+displaynameInput;
          //create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(ckstorifyNode, 'ckstorify', 'ckstorify', false);
          newFakeImage.addClass('ckstorify');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          //reset title
          CKEDITOR.lang.en.fakeobjects.ckstorify = CKEDITOR.socialmedia.ckstorify;
        }
      },
      onShow: function() {
        //Set up to handle existing items
        this.fakeImage = this.ckstorifyNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckstorify') {
          this.fakeImage = fakeImage;
          var ckstorifyNode = editor.restoreRealElement(fakeImage);
          this.ckstorifyNode = ckstorifyNode;
          this.setupContent(ckstorifyNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('storify', function(editor) {
    return storifyDialog(editor);
  });    
})();

(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var twitterDialog = function(editor) {
    return {
      title : 'Twitter Properties',
      minWidth : 625,
      minHeight : 200,
      contents: [{
        id: 'Twitter',
        label: 'Twitter User',
        elements:[{
          type: 'select',
          id: 'twittertypeInput',
          label: 'Twitter widget type:',
          items: [
                   ['Profile', 'profile'],
                   ['Faves', 'faves'],
                   ['List', 'list'],
                   ['Search/Hashtag', 'search'],
                   ['Embedded Tweet', 'tweet'],
                   ['Custom Timeline', 'custom']
                 ],
          required: true,
          setup: function(element) {
                   this.setValue(element.getAttribute('data-type'));
                 },
          onChange: function() {
                      adjustfortype();
                    }
        },{
          type: 'html',
          id: 'description',
          html: '',
        },{
          type: 'text',
          id: 'twitterWidgetID',
          label: 'Twitter widget ID:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-widget-id'));
                 }
        },{
          type: 'text',
          id: 'twitterInput',
          label: 'Twitter user name: @',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-username'));
                 }
        },{
          type: 'text',
          id: 'listInput',
          label: 'List name:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-listname'));
                 }
        },{
          type: 'text',
          id: 'searchInput',
          label: 'Search term or hashtag: (preface hashtags with "#")',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-search'));
                 }
        },{
          type: 'textarea',
          rows: 4,
          cols: 40,
          id: 'tweetInput',
          label: 'Embedded Tweet code from Twitter or individual Tweet URL:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-tweet'));
                 }
        },{
          type: 'text',
          id: 'urlInput',
          label: 'Twitter URL fragment: https://twitter.com/',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-url'));
                 }
        },{
          type: 'text',
          id: 'timelineInput',
          label: 'Timeline name:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-timeline'));
                 }
        }]
      }],
      onShow: function() {
        //Set up to handle existing items
        this.fakeImage = this.cktwitterNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cktwitter') {
          this.fakeImage = fakeImage;
          var cktwitterNode = editor.restoreRealElement(fakeImage);
          this.cktwitterNode = cktwitterNode;
          this.setupContent(cktwitterNode);
        }
        //Set up the dialog box filtering based on type
        adjustfortype();
      },
      onOk: function() {
        //get form information
        twittertype = this.getValueOf('Twitter','twittertypeInput');
        twitterWidgetID = this.getValueOf('Twitter','twitterWidgetID');
        twitterInput = this.getValueOf('Twitter','twitterInput');
        listInput = this.getValueOf('Twitter','listInput');
        searchInput = this.getValueOf('Twitter','searchInput');
        tweetInput = this.getValueOf('Twitter','tweetInput');
        urlInput = this.getValueOf('Twitter','urlInput');
        timelineInput = this.getValueOf('Twitter','timelineInput');
        //let's be nice and clean up some (likely) common mistakes
        if (twitterInput.substr(0,1) == '@') {
          //strip leading "@" from Twitter usernames
          twitterInput = twitterInput.substr(1);
        }
        listInput = listInput.replace(/ /gi,'-'); //replace spaces with "-" in Twitter list names
        //validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!twitterWidgetID && twittertype != 'tweet') {
          errors += "All Twitter widgets must have a widget ID.\r\n"
        }
        if ((twittertype == 'profile' || twittertype == 'faves' || twittertype == 'list') && !CKEDITOR.socialmedia.twitter_username_regex.test(twitterInput)) {
          errors += "You must enter a valid Twitter user name.\r\n";
        }
        if (twittertype == 'list' && !CKEDITOR.socialmedia.twitter_listname_regex.test(listInput)) {
          errors += "You must enter a valid Twitter list name.\r\n";
        }
        if (twittertype == 'search' && !searchInput) {
          errors += "You must enter a valid search term or hashtag.\r\n";
        }
        if (twittertype == 'tweet' && !CKEDITOR.socialmedia.twitter_tweet_regex.test(tweetInput)) {
          errors += "You must enter a valid embedded Tweet code from Twitter or individual Tweet URL.\r\n";
        }
        if (twittertype == 'custom' && !CKEDITOR.socialmedia.twitter_url_regex.test(urlInput)) {
          errors += "You must enter a valid embedded custom timeline URL fragment.\r\n";
        }
        if (twittertype == 'custom' && !timelineInput) {
          errors += "You must enter a valid embedded custom timeline name.\r\n";
        }
        if (twittertype != 'profile' && twittertype != 'faves' && twittertype != 'list' && twittertype != 'search' && twittertype != 'tweet' && twittertype != 'custom') {
          errors = 'Invalid widget type.';
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          //create the cktwitter element
          var cktwitterNode = new CKEDITOR.dom.element('cktwitter');
          //save contents of dialog as attributes of the element
          cktwitterNode.setAttribute('data-type',twittertype);
          cktwitterNode.setAttribute('data-widget-id',twitterWidgetID);
          cktwitterNode.setAttribute('data-username',twitterInput);
          cktwitterNode.setAttribute('data-listname',listInput);
          cktwitterNode.setAttribute('data-search',searchInput);
          cktwitterNode.setAttribute('data-tweet',tweetInput);
          cktwitterNode.setAttribute('data-url',urlInput);
          cktwitterNode.setAttribute('data-timeline',timelineInput);
          //adjust title based on user input
          widgettext = 'Twitter widget: ';
          if (twittertype == 'profile') {
            widgettext += 'Tweets by @'+twitterInput;
          }
          if (twittertype == 'faves') {
            widgettext += 'Favourite Tweets by @'+twitterInput;
          }
          if (twittertype == 'list') {
            widgettext += 'Tweets from @'+twitterInput+'/'+listInput;
          }
          if (twittertype == 'search') {
            widgettext += 'Tweets about "'+searchInput+'"';
          }
          if (twittertype == 'tweet') {
            widgettext += 'Embedded Tweet';
          }
          if (twittertype == 'custom') {
            widgettext += 'Embedded custom timeline "'+timelineInput+'"';
          }
          CKEDITOR.lang.en.fakeobjects.cktwitter = widgettext;
          //create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(cktwitterNode, 'cktwitter', 'cktwitter', false);
      newFakeImage.addClass('cktwitter');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          //reset title
          CKEDITOR.lang.en.fakeobjects.cktwitter = CKEDITOR.socialmedia.cktwitter;
        }
      },
    }
  }

  CKEDITOR.dialog.add('twitter', function(editor) {
    return twitterDialog(editor);
  });

  function adjustfortype() {
    //show and hide form elements based on which Twitter type is selected
    dialog = CKEDITOR.dialog.getCurrent();
    twittertype = dialog.getValueOf('Twitter','twittertypeInput');
    twitterWidgetID = dialog.getContentElement('Twitter','twitterWidgetID').getElement();
    twitterInput = dialog.getContentElement('Twitter','twitterInput').getElement();
    listInput = dialog.getContentElement('Twitter','listInput').getElement();
    searchInput = dialog.getContentElement('Twitter','searchInput').getElement();
    tweetInput = dialog.getContentElement('Twitter','tweetInput').getElement();
    urlInput = dialog.getContentElement('Twitter','urlInput').getElement();
    timelineInput = dialog.getContentElement('Twitter','timelineInput').getElement();
    description = dialog.getContentElement('Twitter','description').getElement();
    if (twittertype == 'profile') {
      twitterWidgetID.show();
      twitterInput.show();
      listInput.hide();
      searchInput.hide();
      tweetInput.hide();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show the user&rsquo;s most recent tweets.');
    } else if (twittertype == 'faves') {
      twitterWidgetID.show();
      twitterInput.show();
      listInput.hide();
      searchInput.hide();
      tweetInput.hide();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show the user&rsquo;s most recent favourited tweets.');
    } else if (twittertype == 'list') {
      twitterWidgetID.show();
      twitterInput.show();
      listInput.show();
      searchInput.hide();
      tweetInput.hide();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show the most recent tweets from a user&rsquo;s public list.');
    } else if (twittertype == 'search') {
      twitterWidgetID.show();
      twitterInput.hide();
      listInput.hide();
      searchInput.show();
      tweetInput.hide();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show the most recent tweets from a search term or hashtag.');
    } else if (twittertype == 'tweet') {
      twitterWidgetID.hide();
      twitterInput.hide();
      listInput.hide();
      searchInput.hide();
      tweetInput.show();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show a specific tweet.');
    } else if (twittertype == 'custom') {
      twitterWidgetID.show();
      twitterInput.hide();
      listInput.hide();
      searchInput.hide();
      tweetInput.hide();
      urlInput.show();
      timelineInput.show();
      description.setHtml('Show an embedded custom timeline.');
    }
  }
})();

//define tags as "block level" so the editor doesn't try to put paragraph tags around them
cktags = ['cktwitter','ckfacebook','cklivestream','ckmailman','ckstorify','cktableau', 'cktint', 'ckvimeo', 'ckmailchimp', 'cktimeline'];

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

/*
for (index = 0; index < cktags.length; index++) {

  CKEDITOR.dtd.$block[cktags[index]] = 1;
  CKEDITOR.dtd.$body[cktags[index]] = 1;
  CKEDITOR.dtd.$empty[cktags[index]] = 1;
  CKEDITOR.dtd.body[cktags[index]] = 1;
  CKEDITOR.dtd[cktags[index]] = {};
}
*/
CKEDITOR.plugins.add('socialmedia', {
  requires : ['dialog', 'fakeobjects'],
  init: function(editor) {
    //Define plugin name
    var pluginName='socialmedia';

    //Set up object to share variables amongst scripts
    CKEDITOR.socialmedia = {};

    //Register button for Twitter
    editor.ui.addButton( 'Twitter', {
      label : "Add/Edit Twitter Feed",
      command : 'twitter',
      icon: this.path+'icons/twitter.png',
    });
    //Register right-click menu item for Twitter
    editor.addMenuItems({
      twitter : {
        label : "Edit Twitter Feed",
        icon: this.path + 'icons/twitter.png',
        command : 'twitter',
        group : 'image',
        order : 1
      }
    });
    // Make cktwitter to be a self-closing tag.
    CKEDITOR.dtd.$empty['cktwitter'] = 1;
    //make sure the fake element for Twitter has a name
    CKEDITOR.socialmedia.cktwitter = 'Twitter widget';
    CKEDITOR.lang.en.fakeobjects.cktwitter = CKEDITOR.socialmedia.cktwitter;
    //Add JavaScript file that defines the dialog box for Twitter
    CKEDITOR.dialog.add('twitter', this.path + 'dialogs/twitter.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('twitter', new CKEDITOR.dialogCommand('twitter'));
    //Regular expressions for Twitter
    CKEDITOR.socialmedia.twitter_username_regex = /^[a-zA-Z0-9_]{1,15}$/;
    CKEDITOR.socialmedia.twitter_listname_regex = /^[a-zA-Z][a-zA-Z0-9_]{0,24}$/;
    CKEDITOR.socialmedia.twitter_tweet_regex = /.*twitter\.com\/([^"\/]+\/status\/[0-9]+)(?:"|&quot;|\/?$)/;
    CKEDITOR.socialmedia.twitter_url_regex = /^[\w\d_]{1,15}\/timelines\/\d+$/;
    //Register button for Facebook
    editor.ui.addButton( 'Facebook', {
      label : "Add/Edit Facebook Feed",
      command : 'facebook',
      icon: this.path+'icons/facebook.png',
    });
    //Register right-click menu item for Facebook
    editor.addMenuItems({
      facebook : {
        label : "Edit Facebook Feed",
        icon: this.path + 'icons/facebook.png',
        command : 'facebook',
        group : 'image',
        order : 1
      }
    });
    // Make ckfacebook to be a self-closing tag.
    CKEDITOR.dtd.$empty['ckfacebook'] = 1;
    //make sure the fake element for Facebook has a name
    CKEDITOR.socialmedia.ckfacebook = 'Facebook widget';
    CKEDITOR.lang.en.fakeobjects.ckfacebook = CKEDITOR.socialmedia.ckfacebook;
    //Add JavaScript file that defines the dialog box for Facebook
    CKEDITOR.dialog.add('facebook', this.path + 'dialogs/facebook.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('facebook', new CKEDITOR.dialogCommand('facebook'));
    //Facebook web addresses can only contain alphanumeric characters (a-z, 0-9) or periods ("."), and must contain at least one letter. You may also include periods and capital letters to make your web address easier to read, but their use by others in finding your Page is optional. For example, http://www.facebook.com/fbsitegovernance and http://www.facebook.com/FB.Site.Governance go to the same Page.
    //Pages don't necessarily follow these rules. Some pages are /pages/pagename/number (e.g. www.facebook.com/pages/Student-Life-Centre/184707270634)
    //*might* be a 75 character limit (we will assume so until proven otherwise)
    CKEDITOR.socialmedia.facebook_username_regex_1 = /[a-zA-Z]/;
    CKEDITOR.socialmedia.facebook_username_regex_2 = /^(?:[a-zA-Z0-9.]{1,75}|pages\/[-.a-zA-Z0-9]+\/[0-9]+)$/;

    //Register button for Livestream
    editor.ui.addButton( 'Livestream', {
      label : "Add/Edit Livestream Video",
      command : 'livestream',
      icon: this.path+'icons/livestream.png',
    });
    //Register right-click menu item for Livestream
    editor.addMenuItems({
      livestream : {
        label : "Edit Livestream Video",
        icon: this.path + 'icons/livestream.png',
        command : 'livestream',
        group : 'image',
        order : 1
      }
    });
    // Make cklivestream to be a self-closing tag.
    CKEDITOR.dtd.$empty['cklivestream'] = 1;
    //make sure the fake element for Livestream has a name
    CKEDITOR.socialmedia.cklivestream = 'Livestream video';
    CKEDITOR.lang.en.fakeobjects.cklivestream = CKEDITOR.socialmedia.cklivestream;
    //Add JavaScript file that defines the dialog box for Livestream
    CKEDITOR.dialog.add('livestream', this.path + 'dialogs/livestream.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('livestream', new CKEDITOR.dialogCommand('livestream'));
    //Regular expressions for Livestream
    CKEDITOR.socialmedia.livestream_username_regex = /^[a-zA-Z0-9_\/]{4,40}$/;

    //Register button for Mailman Mailing Lists
    editor.ui.addButton( 'Mailman', {
      label : "Add/Edit Mailman Subscription Form",
      command : 'mailman',
      icon: this.path+'icons/mailinglists.png',
    });
    //Register right-click menu item for Livestream
    editor.addMenuItems({
      mailman : {
        label : "Edit Mailman Subscription Form",
        icon: this.path + 'icons/mailinglists.png',
        command : 'mailman',
        group : 'image',
        order : 1
      }
    });
    // Make ckmailman to be a self-closing tag.
    CKEDITOR.dtd.$empty['ckmailman'] = 1;
    //make sure the fake element for Mailman Mailing Lists has a name
    CKEDITOR.socialmedia.ckmailman = 'Mailman subscription form';
    CKEDITOR.lang.en.fakeobjects.ckmailman = CKEDITOR.socialmedia.ckmailman;
    //Add JavaScript file that defines the dialog box for Mailman Mailing Lists
    CKEDITOR.dialog.add('mailman', this.path + 'dialogs/mailman.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('mailman', new CKEDITOR.dialogCommand('mailman'));
    //Regular expressions for Mailman Mailing Lists
    /*
    jbg: pax: ACCEPTABLE_LISTNAME_CHARACTERS = '[-+_.=a-z0-9]' <- so "[-+_.=a-z0-9]*@lists\.uwaterloo\.ca"
    pax: jbg: no max/min length?
    jbg: pax, well email means min = 1, max=64...
    jbg: but we don't define anything specific on length..
    ...but http://ist.uwaterloo.ca/~kdpeck/mailman/Mailman.pdf says they can be 256 characters...
    */
    CKEDITOR.socialmedia.mailman_regex = /^[-+_.=a-z0-9]{1,256}$/;

    //Register button for Storify
    editor.ui.addButton( 'Storify', {
      label : "Add/Edit Storify Feed",
      command : 'storify',
      icon: this.path+'icons/storify.png',
    });
    //Register right-click menu item for Storify
    editor.addMenuItems({
      storify : {
        label : "Edit Storify Feed",
        icon: this.path + 'icons/storify.png',
        command : 'storify',
        group : 'image',
        order : 1
      }
    });
    // Make ckstorify to be a self-closing tag.
    CKEDITOR.dtd.$empty['ckstorify'] = 1;
    //make sure the fake element for storify has a name
    CKEDITOR.socialmedia.ckstorify = 'Storify feed';
    CKEDITOR.lang.en.fakeobjects.ckstorify = CKEDITOR.socialmedia.ckstorify;
    //Add JavaScript file that defines the dialog box for Storify
    CKEDITOR.dialog.add('storify', this.path + 'dialogs/storify.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('storify', new CKEDITOR.dialogCommand('storify'));

    //Register button for Tableau
    editor.ui.addButton( 'Tableau', {
      label : "Add/Edit Tableau Visualization",
      command : 'tableau',
      icon: this.path+'icons/tableau.png',
    });
    //Register right-click menu item for Tableau
    editor.addMenuItems({
      tableau : {
        label : "Edit Tableau Visualization",
        icon: this.path + 'icons/tableau.png',
        command : 'tableau',
        group : 'image',
        order : 1
      }
    });
    // Make cktableau to be a self-closing tag.
    CKEDITOR.dtd.$empty['cktableau'] = 1;
    //make sure the fake element for Tableau has a name
    CKEDITOR.socialmedia.cktableau = 'Tableau visualization';
    CKEDITOR.lang.en.fakeobjects.cktableau = CKEDITOR.socialmedia.cktableau;
    //Add JavaScript file that defines the dialog box for Tableau
    CKEDITOR.dialog.add('tableau', this.path + 'dialogs/tableau.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('tableau', new CKEDITOR.dialogCommand('tableau'));

    //Register button for Tint
    editor.ui.addButton( 'Tint', {
      label : "Add/Edit Tint Wall",
      command : 'tint',
      icon: this.path+'icons/tint.png',
    });
    //Register right-click menu item for Tint
    editor.addMenuItems({
      tint : {
        label : "Edit Tint Wall",
        icon: this.path + 'icons/tint.png',
        command : 'tint',
        group : 'image',
        order : 1
      }
    });
    // Make cktint to be a self-closing tag.
    CKEDITOR.dtd.$empty['cktint'] = 1;
    //make sure the fake element for Tint has a name
    CKEDITOR.socialmedia.cktint = 'Tint wall';
    CKEDITOR.lang.en.fakeobjects.cktint = CKEDITOR.socialmedia.cktint;
    //Add JavaScript file that defines the dialog box for Tint
    CKEDITOR.dialog.add('tint', this.path + 'dialogs/tint.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('tint', new CKEDITOR.dialogCommand('tint'));

    //Register button for Vimeo
    editor.ui.addButton( 'Vimeo', {
      label : "Add/Edit Vimeo Video",
      command : 'vimeo',
      icon: this.path+'icons/vimeo.png',
    });
    //Register right-click menu item for Vimeo
    editor.addMenuItems({
      vimeo : {
        label : "Edit Vimeo Video",
        icon: this.path + 'icons/vimeo.png',
        command : 'vimeo',
        group : 'image',
        order : 1
      }
    });
    // Make ckvimeo to be a self-closing tag.
    CKEDITOR.dtd.$empty['ckvimeo'] = 1;
    //make sure the fake element for Vimeo has a name
    CKEDITOR.socialmedia.ckvimeo = 'Vimeo video';
    CKEDITOR.lang.en.fakeobjects.ckvimeo = CKEDITOR.socialmedia.ckvimeo;
    //Add JavaScript file that defines the dialog box for Vimeo
    CKEDITOR.dialog.add('vimeo', this.path + 'dialogs/vimeo.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('vimeo', new CKEDITOR.dialogCommand('vimeo'));

    // Regular expressions for vimeo
    CKEDITOR.socialmedia.vimeo_url_regex = /.*vimeo\.com(.*)/;

    //Register button for MailChimp
    editor.ui.addButton( 'MailChimp', {
      label : "Add/Edit MailChimp",
      command : 'mailchimp',
      icon: this.path+'icons/mailchimp.png',
    });
    //Register right-click menu item for MailChimp
    editor.addMenuItems({
      mailchimp : {
        label : "Edit MailChimp",
        icon: this.path + 'icons/mailchimp.png',
        command : 'mailchimp',
        group : 'image',
        order : 1
      }
    });
    // Make ckmailchimp to be a self-closing tag.
    CKEDITOR.dtd.$empty['ckmailchimp'] = 1;
    //make sure the fake element for MailChimp has a name
    CKEDITOR.socialmedia.ckmailchimp = 'MailChimp';
    CKEDITOR.lang.en.fakeobjects.ckmailchimp = CKEDITOR.socialmedia.ckmailchimp;
    //Add JavaScript file that defines the dialog box for MailChimp
    CKEDITOR.dialog.add('mailchimp', this.path + 'dialogs/mailchimp.js');
    //Register command to open dialog box when button is clicked
    editor.addCommand('mailchimp', new CKEDITOR.dialogCommand('mailchimp'));

    //CKEDITOR.socialmedia.mailchimp_sourcedata_regex = /^[a-zA-Z0-9_\/]{4,40}$/;

    //Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'cktwitter') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Twitter Feed' from the menu, or click on the Twitter icon in the toolbar.");
        evt.data.dialog = 'twitter';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'ckfacebook') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Facebook Feed' from the menu, or click on the Facebook icon in the toolbar.");
        evt.data.dialog = 'facebook';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'cklivestream') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Livestream Video' from the menu, or click on the Livestream icon in the toolbar.");
        evt.data.dialog = 'livestream';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'ckmailman') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Mailman Subscription Form' from the menu, or click on the Mailman icon in the toolbar.");
        evt.data.dialog = 'mailman';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'ckstorify') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Storify Feed' from the menu, or click on the Storify icon in the toolbar.");
        evt.data.dialog = 'storify';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'cktableau') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Tableau Visualization' from the menu, or click on the Tableau icon in the toolbar.");
        evt.data.dialog = 'tableau';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'cktint') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Tint Wall' from the menu, or click on the Tint icon in the toolbar.");
        evt.data.dialog = 'tint';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'ckvimeo') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Vimeo Video' from the menu, or click on the Vimeo icon in the toolbar.");
        evt.data.dialog = 'vimeo';
      } else if (element.is('img') && element.data('cke-real-element-type') === 'ckmailchimp') {
        //alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit MailChimp' from the menu, or click on the MailChimp icon in the toolbar.");
        evt.data.dialog = 'mailchimp';
      }
    });

    //Add the appropriate right-click menu item if an element is right-clicked
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element, selection) {
        if (element && element.is('img') && element.data('cke-real-element-type') === 'cktwitter') {
          return { twitter : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckfacebook') {
          return { facebook : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'cklivestream') {
          return { livestream : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckmailman') {
          return { mailman : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckstorify') {
          return { storify : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'cktableau') {
          return { tableau : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'cktint') {
          return { tint : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckvimeo') {
          return { vimeo : CKEDITOR.TRISTATE_OFF };
        } else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckmailchimp') {
          return { mailchimp : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    //add CSS to use in-editor to style custom (fake) elements

    CKEDITOR.addCss(
      'img.cktwitter {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/twitter.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 350px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      'img.ckfacebook {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/facebook.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 350px;' +
        'display: block;' +
        'clear: both;' +
        'margin: 0 0 10px 0;' +
      '}' +

      'img.cklivestream {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/livestream.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 100%;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      // Livestream: Definitions for wide width
      '.uw_tf_standard_wide img.cklivestream {' +
        'max-height: 460px;' +
      '}' +
      '.uw_tf_standard_wide .col-50 img.cklivestream {' +
        'max-height: 223px;' +
      '}' +
      '.uw_tf_standard_wide .col-33 img.cklivestream {' +
        'max-height: 147px;' +
      '}' +
      '.uw_tf_standard_wide .col-66 img.cklivestream {' +
        'max-height: 297px;' +
      '}' +
      '.uw_tf_standard_wide .threecol-33 img.cklivestream {' +
        'max-height: 147px;' +
      '}' +

      // Livestream: Definitions for standard width
      '.uw_tf_standard img.cklivestream {' +
        'max-height: 307px;' +
      '}' +
      '.uw_tf_standard .col-50 img.cklivestream {' +
        'max-height: 146px;' +
      '}' +
      '.uw_tf_standard .col-33 img.cklivestream {' +
        'max-height: 100px;' +
      '}' +
      '.uw_tf_standard .col-66 img.cklivestream {' +
        'max-height: 196px;' +
      '}' +
      '.uw_tf_standard .threecol-33 img.cklivestream {' +
        'max-height: 100px;' +
      '}'+

      'img.ckmailman {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/mailinglists.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 50px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      'img.ckstorify {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/storify.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 100%;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      // Storify: Definitions for wide width
      '.uw_tf_standard_wide img.ckstorify {' +
        'max-height: 1050px;' +
      '}' +
      '.uw_tf_standard_wide .col-50 img.ckstorify {' +
        'max-height: 504px;' +
      '}' +
      '.uw_tf_standard_wide .col-33 img.ckstorify {' +
        'max-height: 336px;' +
      '}' +
      '.uw_tf_standard_wide .col-66 img.ckstorify {' +
        'max-height: 672px;' +
      '}' +
      '.uw_tf_standard_wide .threecol-33 img.ckstorify {' +
        'max-height: 326px;' +
      '}' +

      // Storify: Definitions for standard width
      '.uw_tf_standard img.ckstorify {' +
        'max-height: 700px;' +
      '}' +
      '.uw_tf_standard .col-50 img.ckstorify {' +
        'max-height: 336px;' +
      '}' +
      '.uw_tf_standard .col-33 img.ckstorify {' +
        'max-height: 228px;' +
      '}' +
      '.uw_tf_standard .col-66 img.ckstorify {' +
        'max-height: 448px;' +
      '}' +
      '.uw_tf_standard .threecol-33 img.ckstorify {' +
        'max-height: 217px;' +
      '}' +

      'img.cktableau {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/tableau.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        //'height: 100px;' +
        'display: block;' +
        'clear: both;' +
        'margin: 0 0 10px 0;' +
      '}' +

      'img.cktint {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/tint.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 250px;' +
        'display: block;' +
        'clear: both;' +
        'margin: 0 0 10px 0;' +
      '}' +

      'img.ckvimeo {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/vimeo.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 100%;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' + 

      'img.ckmailchimp {' +
      'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/mailchimp.png') + ');' +
      'background-position: center center;' +
      'background-repeat: no-repeat;' +
      'background-color: #000;' +
      'width: 100%;' +
      'height: 50px;' +
      'margin: 0 0 10px 0;' +
      'margin-left: auto;' +
      'margin-right: auto;' +
    '}' + 

       // Facebook: Definitions for wide width
      '.uw_tf_standard_wide p img.ckfacebook {' +
        'height: 350px;' +
      '}' +

      // Facebook: Definitions for standard width
      '.uw_tf_standard p img.ckfacebook {' +
        'height: 350px;' +
      '}' +

      // Twitter: Definitions for wide width
      '.uw_tf_standard_wide p img.cktwitter {' +
        'height: 350px;' +
      '}' +

      // Twitter: Definitions for standard width
      '.uw_tf_standard p img.cktwitter {' +
        'height: 350px;' +
      '}' +

      // Mailman: Definitions for wide width
      '.uw_tf_standard_wide p img.ckmailman {' +
        'height: 50px;' +
      '}' +

      // Mailman: Definitions for standard width
      '.uw_tf_standard p img.ckmailman {' +
        'height: 50px;' +
      '}' +

      // Vimeo: Definitions for wide width
      '.uw_tf_standard_wide img.ckvimeo {' +
        'max-height: 383px;' +
      '}' +
      '.uw_tf_standard_wide .col-50 img.ckvimeo {' +
        'max-height: 184px;' +
      '}' +
      '.uw_tf_standard_wide .col-33 img.ckvimeo {' +
        'max-height: 123px;' +
      '}' +
      '.uw_tf_standard_wide .col-66 img.ckvimeo {' +
        'max-height: 245px;' +
      '}' +
      '.uw_tf_standard_wide .threecol-33 img.ckvimeo {' +
        'max-height: 120px;' +
      '}' + 

      // Vimeo: Definitions for standard width
      '.uw_tf_standard img.ckvimeo {' +
        'max-height: 250px;' +
      '}' +
      '.uw_tf_standard .col-50 img.ckvimeo {' +
        'max-height: 123px;' +
      '}' +
      '.uw_tf_standard .col-33 img.ckvimeo {' +
        'max-height: 110px;' +
      '}' +
      '.uw_tf_standard .col-66 img.ckvimeo {' +
        'max-height: 160px;' +
      '}' +
      '.uw_tf_standard .threecol-33 img.ckvimeo {' +
        'max-height: 80px;' +
      '}' +

        // MailChimp: Definitions for wide width
        '.uw_tf_standard_wide p img.ckmailchimp {' +
          'height: 50px;' +
        '}' +

        // MailChimp: Definitions for standard width
        '.uw_tf_standard p img.ckmailchimp {' +
          'height: 50px;' +
        '}'
    );
  },
  afterInit : function (editor) {
    //make fake image display on first load/return from source view
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          cktwitter : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.cktwitter = CKEDITOR.socialmedia.cktwitter;
            twittertype = element.attributes['data-type'];
            widgettext = 'Twitter widget: ';
            if (twittertype == 'profile') {
              widgettext += 'Tweets by @'+element.attributes['data-username'];
            }
            if (twittertype == 'faves') {
              widgettext += 'Favourite Tweets by @'+element.attributes['data-username'];
            }
            if (twittertype == 'list') {
              widgettext += 'Tweets from @'+element.attributes['data-username']+'/'+element.attributes['data-listname'];
            }
            if (twittertype == 'search') {
              widgettext += 'Tweets about "'+element.attributes['data-search']+'"';
            }
            if (twittertype == 'tweet') {
              widgettext += 'Embedded Tweet';
            }
            CKEDITOR.lang.en.fakeobjects.cktwitter = widgettext;
            //note that this just accepts whatever attributes are on the element; may want to filter these
            return editor.createFakeParserElement(element, 'cktwitter', 'cktwitter', false);
          },
          ckfacebook : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.ckfacebook = CKEDITOR.socialmedia.ckfacebook;
            //adjust title if a display name is present
            if (element.attributes['data-displayname']) {
              CKEDITOR.lang.en.fakeobjects.ckfacebook += ': ' + element.attributes['data-displayname'];
            }
            //note that this just accepts whatever attributes are on the element; may want to filter these
            return editor.createFakeParserElement(element, 'ckfacebook', 'ckfacebook', false);
          },
          cklivestream : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.cklivestream = CKEDITOR.socialmedia.cklivestream;
            //adjust title if a display name is present
            if (element.attributes['data-displayname']) {
              CKEDITOR.lang.en.fakeobjects.cklivestream += ': '+element.attributes['data-displayname'];
            }
            //note that this just accepts whatever attributes are on the element; may want to filter these
            return editor.createFakeParserElement(element, 'cklivestream', 'cklivestream', false);
          },
          ckmailman : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.ckmailman = CKEDITOR.socialmedia.ckmailman;
            //adjust title if a list name is present
            if (element.attributes['data-listname']) {
              CKEDITOR.lang.en.fakeobjects.ckmailman += ': ';
              if (element.attributes['data-servername']) {
                CKEDITOR.lang.en.fakeobjects.ckmailman += element.attributes['data-servername']+'/';
              }
              CKEDITOR.lang.en.fakeobjects.ckmailman += element.attributes['data-listname'];
            }
            //note that this just accepts whatever attributes are on the element; may want to filter these
            return editor.createFakeParserElement(element, 'ckmailman', 'ckmailman', false);
          },
          ckstorify : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.ckstorify = CKEDITOR.socialmedia.ckstorify;
            //adjust title if a list name is present
            if (element.attributes['data-story']) {
              CKEDITOR.lang.en.fakeobjects.ckstorify += ': ' + element.attributes['data-story'];
            }
            //note that this just accepts whatever attributes are on the element; may want to filter these
            return editor.createFakeParserElement(element, 'ckstorify', 'ckstorify', false);
          },
          cktableau : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.cktableau = CKEDITOR.socialmedia.cktableau;
            //adjust title if a list name is present
            if (element.attributes['data-url']) {
              CKEDITOR.lang.en.fakeobjects.cktableau += ': ' + element.attributes['data-url'];
            }
            //note that this just accepts whatever attributes are on the element; may want to filter these
            tableau_return = editor.createFakeParserElement(element, 'cktableau', 'cktableau', false);
            //set the fake object to the entered height; if there isn't one, use 100 so it's not invisible
            if (!element.attributes['data-height']) {
              element.attributes['data-height'] = '100';
            }
            tableau_return.attributes.height = element.attributes['data-height'];
            return tableau_return;
          },
          cktint : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cktint = CKEDITOR.socialmedia.cktint;
            // Adjust title if a list name is present.
            if (element.attributes['data-id']) {
              CKEDITOR.lang.en.fakeobjects.cktint += ': ' + element.attributes['data-id'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            tint_return = editor.createFakeParserElement(element, 'cktint', 'cktint', false);
            // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
            if (!element.attributes['data-height']) {
              element.attributes['data-height'] = '100';
            }
            tint_return.attributes.height = element.attributes['data-height'];
            return tint_return;
          },
          ckvimeo : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.ckvimeo = CKEDITOR.socialmedia.ckvimeo;
            //adjust title if a list name is present
            if (element.attributes['data-url']) {
              CKEDITOR.lang.en.fakeobjects.ckvimeo += ': ' + element.attributes['data-url'];
            }
            //note that this just accepts whatever attributes are on the element; may want to filter these
            return editor.createFakeParserElement(element, 'ckvimeo', 'ckvimeo', false);
          },
          ckmailchimp : function (element) {
            //reset title
            CKEDITOR.lang.en.fakeobjects.ckmailchimp = CKEDITOR.socialmedia.ckmailchimp;
            //adjust title if a list name is present
            if (element.attributes['data-sourcecode']) {
              CKEDITOR.lang.en.fakeobjects.ckmailchimp += ': ';
              if (element.attributes['data-sourcecode']) {
                CKEDITOR.lang.en.fakeobjects.ckmailchimp += element.attributes['data-sourcecode'] + '/';
              }
              CKEDITOR.lang.en.fakeobjects.ckmailchimp += element.attributes['data-sourcecode'];
            }
            //note that this just accepts whatever attributes are on the element; may want to filter these
            return editor.createFakeParserElement(element, 'ckmailchimp', 'ckmailchimp', false);
          }
        }
      });
    }
  }
});

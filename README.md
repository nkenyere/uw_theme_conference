# UW Theme Conference 

A custom frontend for WCMS-based conference sites. This frontend is built with AngularJS 1.4.x and Bootstrap 3.3.x. Project and development dependencies are managed with Bower and the project is built with Gulp. This theme is integrated with Drupal via the UW Conference Frontend project which provides a Drupal theme wrapper for this custom theme.  

## Getting Started (for development)

```
bower install 
npm install 
gulp watch 
```

## Getting Started (for production)

Create a symlink to this directory from the Drupal root directory. 

```
ln -S <path_to_drupal>/profiles/uw_base_profile/themes/uw_theme_conference <path_to_drupal>/apps
```
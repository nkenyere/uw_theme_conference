'use strict';

// Dependencies
var gulp     = require('gulp');
var sass     = require('gulp-sass');
var concat   = require('gulp-concat');
var maps     = require('gulp-sourcemaps');
var annotate = require('gulp-ng-annotate');
var sequence = require('gulp-run-sequence');
var cache    = require('gulp-angular-templatecache');
var uglify   = require('gulp-uglify');
var rename   = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

// Constants
var BOWER     = './bower_components';
var SRC       = './assets/src';
var DIST      = './assets/dist';
var IMG_SRC   = SRC + '/img';
var IMG_DIST  = DIST + '/img';
var JS_SRC    = SRC + '/js';
var JS_DIST   = DIST + '/js';
var SASS_SRC  = SRC + '/scss';
var SASS_DIST = DIST + '/css';
var TPL_SRC   = './templates';
var TPL_DIST  = JS_SRC;
var JS_SRCS   = [
  // Libraries
  JS_SRC + '/vendor/xhook.min.js',
  BOWER  + '/jquery/dist/jquery.min.js',
  BOWER  + '/lodash/lodash.min.js',
  BOWER  + '/moment/min/moment.min.js',
  BOWER  + '/gsap/src/minified/TweenMax.min.js',
  JS_SRC + '/vendor/twitter.js',
  JS_SRC + '/vendor/twitter-txt.js',

  // Angular Libraries
  BOWER  + '/angular/angular.js',
  BOWER  + '/angular-aria/angular-aria.min.js',
  BOWER  + '/angular-animate/angular-animate.min.js',
  BOWER  + '/ngFx/dist/ngFx.min.js',
  BOWER  + '/angular-bootstrap/ui-bootstrap.min.js',
  BOWER  + '/angular-bootstrap/ui-bootstrap-tpls.min.js',
  BOWER  + '/angular-ui-event/dist/event.min.js',
  BOWER  + '/ngmap/build/scripts/ng-map.min.js',
  BOWER  + '/angular-ui-router/release/angular-ui-router.min.js',
  BOWER  + '/angular-ui-router-anim-in-out/anim-in-out.js',
  JS_SRC + '/vendor/slick.custom.js',
  BOWER  + '/angular-slick/dist/slick.min.js',
  BOWER  + '/angular-socialshare/dist/angular-socialshare.min.js',
  BOWER  + '/jquery.fitvids/jquery.fitvids.js',

  // App
  JS_SRC + '/main.js',
  JS_SRC + '/templates.js',
  JS_SRC + '/app/**/*.js'
];

// Compress images in source and move to destination
function imgHandler() {
  return gulp
    .src(IMG_SRC + '/**/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(IMG_DIST))
  ;
}

// Compile sass files in source and move output to destination
function sassHandler() {
  return gulp
    .src(SASS_SRC + '/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(SASS_DIST))
    ;
}

// Concat and minify files in source and move output to destination
function jsHandler() {
  return gulp
    .src(JS_SRCS)
    .pipe(maps.init())
    .pipe(concat({path: 'main.js', stat: {mode: '0666'}}))
    .pipe(annotate())
    .pipe(maps.write())
    .pipe(gulp.dest(JS_DIST))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest(JS_DIST))
  ;
}

// Push templates files into strings and move to destination
function tplHandler() {
  return gulp
    .src(TPL_SRC + '/**/*.tpl.html')
    .pipe(cache())
    .pipe(gulp.dest(TPL_DIST))
  ;
}

// Watch for changes to files in source and run respective tasks
function watchHandler() {
  gulp.watch(IMG_SRC + '/**/*', ['images']);
  gulp.watch(SASS_SRC + '/**/*.scss', ['sass']);
  gulp.watch(JS_SRC + '/**/*.js', ['scripts']);
  gulp.watch(TPL_SRC + '/**/*.tpl.html', ['templates']);
}

// Build the application
function buildHandler() {
  sequence(['images', 'sass'], 'templates', 'scripts');
}

// Tasks
gulp.task('images', imgHandler);
gulp.task('sass', sassHandler);
gulp.task('scripts', jsHandler);
gulp.task('templates', tplHandler);
gulp.task('watch', watchHandler);
gulp.task('default', buildHandler);
